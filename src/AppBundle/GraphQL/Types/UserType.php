<?php
namespace AppBundle\GraphQL\Types;

use Youshido\GraphQL\Type\Object\AbstractObjectType;
use Youshido\GraphQL\Type\Scalar\IdType;
use Youshido\GraphQL\Type\Scalar\StringType;

class UserType extends AbstractObjectType
{
    public function build($config)
    {
        $config
            ->addField('id', new IdType())
            ->addField('username', new StringType())
            ->addField('name', new StringType());
    }

    public function getName()
    {
        return "User";
    }
}
