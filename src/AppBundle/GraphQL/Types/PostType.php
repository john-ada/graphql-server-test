<?php
namespace AppBundle\GraphQL\Types;

use Youshido\GraphQL\Type\Object\AbstractObjectType;
use Youshido\GraphQL\Type\Scalar\IdType;
use Youshido\GraphQL\Type\Scalar\StringType;
use AppBundle\GraphQL\Types\UserType;

class PostType extends AbstractObjectType
{
    public function build($config)
    {
        $config
            ->addField('id', new IdType())
            ->addField('title', new StringType())
            ->addField('content', new StringType())
            ->addField('author', new UserType());
    }

    public function getName()
    {
        return "Post";
    }
}
