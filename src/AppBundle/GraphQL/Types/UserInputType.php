<?php
namespace AppBundle\GraphQL\Types;

use Youshido\GraphQL\Type\Config\InputTypeConfigInterface;
use Youshido\GraphQL\Type\NonNullType;
use Youshido\GraphQL\Type\InputObject\AbstractInputObjectType;
use Youshido\GraphQL\Type\Scalar\StringType;

class UserInputType extends AbstractInputObjectType
{
    public function build($config)
    {
        $config
            ->addField('username', new NonNullType(new StringType()))
            ->addField('password', new NonNullType(new StringType()))
            ->addField('name', new NonNullType(new StringType()));
    }

}
