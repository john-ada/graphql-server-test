<?php
namespace AppBundle\GraphQL\Types;

use Youshido\GraphQL\Type\Config\InputTypeConfigInterface;
use Youshido\GraphQL\Type\NonNullType;
use Youshido\GraphQL\Type\InputObject\AbstractInputObjectType;
use Youshido\GraphQL\Type\Scalar\StringType;
use Youshido\GraphQL\Type\Scalar\IdType;

class PostInputType extends AbstractInputObjectType
{
    public function build($config)
    {
        $config
            ->addField('title', new NonNullType(new StringType()))
            ->addField('author', new NonNullType(new IdType()))
            ->addField('content', new NonNullType(new StringType()));
    }

}
