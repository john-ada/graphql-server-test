<?php
namespace AppBundle\GraphQL\Fields;

use Youshido\GraphQL\Execution\ResolveInfo;
use Youshido\GraphQLBundle\Field\AbstractContainerAwareField;
use Youshido\GraphQL\Config\Field\FieldConfig;
use Youshido\GraphQL\Type\NonNullType;
use Youshido\GraphQL\Type\Scalar\IdType;
use Youshido\GraphQL\Type\Scalar\StringType;

use AppBundle\GraphQL\Types\UserType;
use AppBundle\GraphQL\Types\UserInputType;

use AppBundle\Entity\User;

class CreateUserField extends AbstractContainerAwareField
{
    public function resolve($value, array $args, ResolveInfo $info)
    {
        $user = new User();
        $user->setUsername($args['user']['username']);
        $user->setPassword($args['user']['password']); // TODO: Hash
        $user->setName($args['user']['name']);

        $em = $this->container->get('doctrine')->getManager();
        $em->persist($user);
        $em->flush();

        return [
            "id"         => $user->getId(),
            "username"   => $user->getUsername(),
            "name"       => $user->getName(),
        ];
    }

    public function build(FieldConfig $config)
    {
        $config->addArgument('user', new UserInputType());
    }

    public function getType()
    {
        return new UserType();
    }
}
