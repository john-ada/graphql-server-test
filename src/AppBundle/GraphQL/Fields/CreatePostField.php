<?php
namespace AppBundle\GraphQL\Fields;

use Youshido\GraphQL\Execution\ResolveInfo;
use Youshido\GraphQLBundle\Field\AbstractContainerAwareField;
use Youshido\GraphQL\Config\Field\FieldConfig;
use Youshido\GraphQL\Type\NonNullType;
use Youshido\GraphQL\Type\Scalar\IdType;
use Youshido\GraphQL\Type\Scalar\StringType;

use AppBundle\GraphQL\Types\PostType;
use AppBundle\GraphQL\Types\PostInputType;

use AppBundle\Entity\Post;
use AppBundle\Entity\User;

class CreatePostField extends AbstractContainerAwareField
{
    public function resolve($value, array $args, ResolveInfo $info)
    {
        $post = new Post();
        $post->setTitle($args['post']['title']);
        $post->setContent($args['post']['content']);

        $id = $args['post']['author'];
        $author = $this->container->get('user_repo')->find($id);

        $post->setAuthor($author);

        $em = $this->container->get('doctrine')->getManager();
        $em->persist($post);
        $em->flush();

        return [
            "id"      => $post->getId(),
            "title"   => $post->getTitle(),
            "content" => $post->getContent(),
            "author"  => $this->resolveAuthor($post->getAuthor()),
        ];
    }

    private function resolveAuthor(User $user)
    {
        return [
            "id"       => $user->getId(),
            "username" => $user->getUsername(),
            "name"     => $user->getName(),
        ];
    }

    public function build(FieldConfig $config)
    {
        $config->addArgument('post', new PostInputType());
    }

    public function getType()
    {
        return new PostType();
    }
}
