<?php
namespace AppBundle\GraphQL\Fields;

use Youshido\GraphQL\Execution\ResolveInfo;
use Youshido\GraphQL\Config\Field\FieldConfig;
use AppBundle\GraphQL\Types\PostType;
use Youshido\GraphQL\Type\NonNullType;
use Youshido\GraphQL\Type\Scalar\IdType;
use Youshido\GraphQLBundle\Field\AbstractContainerAwareField;
use AppBundle\Entity\User;

class PostField extends AbstractContainerAwareField
{
    public function resolve($value, array $args, ResolveInfo $info)
    {
        $post = $this->container->get('post_repo')->find($args['id']);

        return [
            "id"      => $post->getId(),
            "title"   => $post->getTitle(),
            "content" => $post->getContent(),
            "author"  => $this->resolveAuthor($post->getAuthor()),
        ];
    }

    private function resolveAuthor(User $user)
    {
        return [
            "id"       => $user->getId(),
            "username" => $user->getUsername(),
            "name"     => $user->getName(),
        ];
    }

    public function build(FieldConfig $config)
    {
        $config->addArgument('id', new NonNullType(new IdType()));
    }

    public function getType()
    {
        return new PostType();
    }
}
