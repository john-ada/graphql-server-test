<?php
namespace AppBundle\GraphQL\Fields;

use Youshido\GraphQL\Execution\ResolveInfo;
use Youshido\GraphQL\Config\Field\FieldConfig;
use AppBundle\GraphQL\Types\UserType;
use Youshido\GraphQL\Type\NonNullType;
use Youshido\GraphQL\Type\Scalar\IdType;
use Youshido\GraphQLBundle\Field\AbstractContainerAwareField;

class UserField extends AbstractContainerAwareField
{
    public function resolve($value, array $args, ResolveInfo $info)
    {
        $user = $this->container->get('user_repo')->find($args['id']);

        return [
            "id"         => $user->getId(),
            "username"   => $user->getUsername(),
            "name"       => $user->getName(),
        ];
    }

    public function build(FieldConfig $config)
    {
        $config->addArgument('id', new NonNullType(new IdType()));
    }

    public function getType()
    {
        return new UserType();
    }
}
